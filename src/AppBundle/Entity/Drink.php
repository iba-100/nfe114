<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Drink
 *
 * @ORM\Table(name="drink")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DrinkRepository")
 */
class Drink
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="alcohol", type="boolean")
     */
    private $alcohol;

    /**
     * @var int
     *
     * @ORM\Column(name="alcoholVolume", type="smallint")
     */
    private $alcoholVolume;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="cl", type="smallint")
     */
    private $cl;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Drink
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alcohol
     *
     * @param boolean $alcohol
     *
     * @return Drink
     */
    public function setAlcohol($alcohol)
    {
        $this->alcohol = $alcohol;

        return $this;
    }

    /**
     * Get alcohol
     *
     * @return bool
     */
    public function getAlcohol()
    {
        return $this->alcohol;
    }

    /**
     * Set alcoholVolume
     *
     * @param integer $alcoholVolume
     *
     * @return Drink
     */
    public function setAlcoholVolume($alcoholVolume)
    {
        $this->alcoholVolume = $alcoholVolume;

        return $this;
    }

    /**
     * Get alcoholVolume
     *
     * @return int
     */
    public function getAlcoholVolume()
    {
        return $this->alcoholVolume;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Drink
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set cl
     *
     * @param integer $cl
     *
     * @return Drink
     */
    public function setCl($cl)
    {
        $this->cl = $cl;

        return $this;
    }

    /**
     * Get cl
     *
     * @return int
     */
    public function getCl()
    {
        return $this->cl;
    }
}

