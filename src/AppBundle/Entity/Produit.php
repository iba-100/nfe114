<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="homemade", type="boolean")
     */
    private $homemade;

    /**
     * @var bool
     *
     * @ORM\Column(name="isVegan", type="boolean")
     */
    private $isVegan;

    /**
     * @var int
     *
     * @ORM\Column(name="temperature", type="smallint")
     */
    private $temperature;

    /**
     *@ORM\OneToOne(targetEntity="AppBundle\Entity\Ingredient", cascade = {"persist"})
     */
    private $ingredients;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Produit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Produit
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set homemade
     *
     * @param boolean $homemade
     *
     * @return Produit
     */
    public function setHomemade($homemade)
    {
        $this->homemade = $homemade;

        return $this;
    }

    /**
     * Get homemade
     *
     * @return bool
     */
    public function getHomemade()
    {
        return $this->homemade;
    }

    /**
     * Set isVegan
     *
     * @param boolean $isVegan
     *
     * @return Produit
     */
    public function setIsVegan($isVegan)
    {
        $this->isVegan = $isVegan;

        return $this;
    }

    /**
     * Get isVegan
     *
     * @return bool
     */
    public function getIsVegan()
    {
        return $this->isVegan;
    }

    /**
     * Set temperature
     *
     * @param integer $temperature
     *
     * @return Produit
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature
     *
     * @return int
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set ingredients
     *
     * @param \AppBundle\Entity\Ingredient $ingredients
     *
     * @return Produit
     */
    public function setIngredients(\AppBundle\Entity\Ingredient $ingredients = null)
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * Get ingredients
     *
     * @return \AppBundle\Entity\Ingredient
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }
}
