<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Drink;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Drink controller.
 *
 */
class DrinkController extends Controller
{
    /**
     * Lists all drink entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $drinks = $em->getRepository('AppBundle:Drink')->findAll();

        return $this->render('drink/index.html.twig', array(
            'drinks' => $drinks,
        ));
    }

    /**
     * Creates a new drink entity.
     *
     */
    public function newAction(Request $request)
    {
        $drink = new Drink();
        $form = $this->createForm('AppBundle\Form\DrinkType', $drink);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($drink);
            $em->flush();

            return $this->redirectToRoute('drink_show', array('id' => $drink->getId()));
        }

        return $this->render('drink/new.html.twig', array(
            'drink' => $drink,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a drink entity.
     *
     */
    public function showAction(Drink $drink)
    {
        $deleteForm = $this->createDeleteForm($drink);

        return $this->render('drink/show.html.twig', array(
            'drink' => $drink,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing drink entity.
     *
     */
    public function editAction(Request $request, Drink $drink)
    {
        $deleteForm = $this->createDeleteForm($drink);
        $editForm = $this->createForm('AppBundle\Form\DrinkType', $drink);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('drink_edit', array('id' => $drink->getId()));
        }

        return $this->render('drink/edit.html.twig', array(
            'drink' => $drink,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a drink entity.
     *
     */
    public function deleteAction(Request $request, Drink $drink)
    {
        $form = $this->createDeleteForm($drink);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($drink);
            $em->flush();
        }

        return $this->redirectToRoute('drink_index');
    }

    /**
     * Creates a form to delete a drink entity.
     *
     * @param Drink $drink The drink entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Drink $drink)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('drink_delete', array('id' => $drink->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
